Puzzler 2019
============

This is my submission for the 2019 Christmas Puzzler.
It is done in a language that is well known for LISt Processing.

Rules
-----

 * Generate word squares of M x N cells
 * Each cell must contain two letters or no letters
 * 50% or more of the cells in each row and column must be filled
 * At least one cell in each row and column must be blank
 * Each row and column must contain precisely one word
 * Don't repeat words anywhere
 * Use uncapitalised words from /usr/share/dict/words
 * Any language, output format, etc. is fine
 * No crying

Details
-------

It's done in SQL.

Endorsements
------------

> Matt is banned from live - Alexey (probably)

> Ok, that is insane - Basic Matt (certainly)

> And people thought you wouldn't be able to top the vim submission - Paul

Running
-------

There are scripts to make the running this easier:

```bash
bin/build-docker-image
bin/run-pg
```

Then, in another terminal:

```bash
bin/load-schema
bin/3x3 | bin/prettify
bin/4x4 | bin/prettify
bin/5x5
```

The 5x5 solution takes a while to complete.

### Running Manually

Build the docker image:

```bash
docker build --tag pg-python .
```

Run the docker image:

```bash
docker run --rm --name pg -p 5432:5432 pg-python
```

This implementation is so efficient that it may choose to generate terabytes of temporary files.
If this concerns you then you can mount the data directory to a separate disk with the following (replace `$PWD/data` with the path to the separate disk):

```bash
docker run --rm --name pg -p 5432:5432 -v $PWD/data:/var/lib/postgresql/data pg-python
```

Load the schema and functions:

```bash
psql -p 5432 -h localhost -U postgres postgres -f sql/schema.sql
psql -p 5432 -h localhost -U postgres postgres -f sql/functions.sql
```

Get yourself a bunch of solutions:

```bash
psql -p 5432 -h localhost -U postgres postgres -f sql/words.sql -v width=3 -v height=3 -v limit=10000 -v minimum_frequency=100
```

Make them pretty:

```bash
psql -p 5432 -h localhost -U postgres postgres -f sql/words.sql -v width=3 -v height=3 -v limit=10000 -v minimum_frequency=100 | head -n-3 | sed -e 1d -e 2d -e 's/\n/\n\n/' -e 's/$/\
/g' -e 's/\\n/\
 /g'
```

The `bin/prettify` script can receive the output of a query to turn it into word grids:

```bash
psql -p 5432 -h localhost -U postgres postgres -f sql/words.sql -v width=3 -v height=3 -v limit=10000 -v minimum_frequency=100 | bin/prettify
```

Exposition
----------

Here I explain this solution a bit more, for those that are interested.
You can also review the notebooks to see how this developed.

### Database Design

This uses PostgreSQL with a Data Warehouse star schema.
The Data Warehouse is organized with the pair-per-word as the fact table (named entries).

```sql
TABLE entries (
    pair_id INTEGER REFERENCES pairs(id),
    word_id INTEGER REFERENCES words(id),
    position INTEGER,
    CONSTRAINT distinct_entries UNIQUE(pair_id, word_id, position)
);
```

This then links to the distinct pairs as a dimension.
Pairs know their frequency in the entries table, as that helps filter out any pairs that could never be used in a word grid.

```sql
TABLE pairs (
    id SERIAL PRIMARY KEY,
    letters TEXT UNIQUE NOT NULL,
    frequency INTEGER NOT NULL
);
```

The entries table also links to the distinct words.
Words know the minimum frequency of any pair that occurs within them, as that helps select words likely to form grids easily.

```sql
TABLE words (
    id SERIAL PRIMARY KEY,
    word TEXT NOT NULL,
    length INTEGER,
    minimum_frequency INTEGER
);
```

### 3x3 Word Grid Generation

The initial version of word grid generation just asked for a valid 3x3 grid in a single relatively simple statement.
When this was executed it ran for many hours and generated >700G of temporary files before I cancelled it.
When I reviewed the plan for the query I noticed that it was pretty fresh, with 13 nested nested loops and one of those anticipated processing more than 1 billion rows.

So I needed to guide the planner a little.
The approach I tried first is:

Generate a central row (A):

    .. .. ..
    xx .. xx
    .. .. ..

Generate the columns that intersect with A (Bs):

    .. .. xx
    XX .. XX
    xx .. ..

Generate the rows that intersect with Bs (Cs):

    .. xx XX
    .. .. ..
    XX xx ..

Validate the column that intersects with Cs (D):

    .. XX ..
    .. .. ..
    .. XX ..

This approach worked well and with appropriate use of LIMIT it was able to generate thousands of 3x3 word grids in under a second.

I did not add any way to alter the arrangement of blanks, the diagonal stripe of them produces the most dense grid and is valid for any square grid.
I think that allowing the blanks to vary would add so much freedom in picking a solution that it would be nearly impossible to come up with a query that would complete in a reasonable time.
This does constrain the valid solutions to squares, however I first need to be able to generate larger grids...

### NxN Word Grid Generation

I then faced the problem of how to make this generic and able to accept different widths and heights.
Two approaches occurred to me.

The first was that Common Table Expressions could be used to generate and validate each cell in turn.
This can work because PostgreSQL supports recursive CTE, which would allow me to loop over all of the grid.
The immediate problem with this is that every possible pair for the first cell would have to be generated before any pair for the second cell could be considered.
Given the problem with the naïve query that I originally wrote I did not think that this would complete.
I have found it useful to think of the query as a breadth first search - every possible first step must be considered before any second step.
Taking larger steps helps, and limiting the number of steps to consider also helps.

The second approach was to generate the SQL query by writing a query that produces an SQL query based on the width and height.
The same CTE would allow me to step through the different parts of the select, joins and where clauses building them up in turn.
This started well but quickly became unmanageable.
The performance of the SQL generating query also became a concern when I wrote a nested CTE.

So I resorted to writing the SQL generating code in Python.
I was up against the time limit, and the Python is in the database (it is within a plpython3u function).
It is long and reasonably simple.

The one significant change to the original approach is that the first row that is generated is the top row.
This way I do not have to calculate the appropriate middle row to use.

### Performance

The performance of the 3x3 grid is good.
On my machine it generates 8,987 grids in 765 ms.

The performance of the 4x4 grid is also ok.
On my machine it generates 502 grids in 2197 ms.

When you move past that it becomes difficult to generate a valid grid with the default settings.
You have to consider more intermediate states as the number of potential grids that drop out is quite high.
This means it becomes quite slow.

The SQL generation can produce rectangular grids.
They are also quite slow.
I have not investigated this as the rectangular grids are invalid submissions.
