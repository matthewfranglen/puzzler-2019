from __future__ import annotations
from contextlib import contextmanager
from typing import Any, List
import psycopg2


@contextmanager
def connect():
    with _connection() as connection:
        cursor = connection.cursor()
        try:
            yield cursor
        finally:
            connection.commit()
            cursor.close()


@contextmanager
def _connection():
    connection = psycopg2.connect(
        dbname="postgres", user="postgres", host="localhost", port=5432
    )
    try:
        yield connection
    finally:
        connection.close()


def execute(statement: str, *args) -> None:
    with connect() as cursor:
        cursor.execute(statement, *args)


def select(statement: str, *args) -> List[Any]:
    with connect() as cursor:
        cursor.execute(statement, *args)
        return cursor.fetchall()


def vacuum() -> None:
    with _connection() as connection:
        connection.set_session(autocommit=True)
        with connection.cursor() as cursor:
            cursor.execute("""VACUUM FULL""")
