# pylint: disable=invalid-name


def generate_sql(
    width: int, height: int, limit: int = 10_000, minimum_frequency: int = 100
) -> str:
    inner_table = (
        "\n\t".join(
            _generate_col_d(width, height, limit, minimum_frequency).splitlines()
        )
        + "\n"
    )
    return f"""
SELECT
\t{_generate_outer_select(width, height)}
FROM ({inner_table}) col_d
"""


def _generate_outer_select(width: int, height: int) -> str:
    return " || '\\n' || \n\t".join(
        " || ".join(
            "    '..'   " if w == h else f"letters_{h}_{w}" for w in range(width)
        )
        for h in range(height)
    )


def _generate_col_d(width: int, height: int, limit: int, minimum_frequency: int) -> str:
    """
        This generates the query for Column D which intersects with Rows Cs:
        .. .. ..
        XX .. ..
        XX .. ..
    """
    inner_table = (
        "\n\t".join(
            _generate_rows_cs(width, height, limit, minimum_frequency).splitlines()
        )
        + "\n"
    )
    return f"""
SELECT
\t{_generate_col_d_select(width, height)}
FROM ({inner_table}) rows_cs
{_generate_col_d_join(width, height)}
{_generate_col_d_where(width, height, minimum_frequency)}
""" + _limit(
        limit
    )


def _limit(limit: int) -> str:
    return f"LIMIT {limit}" if limit > 0 else ""


def _generate_col_d_select(width: int, height: int) -> str:
    return ",\n\t".join(
        f"pair_{h}_{w}.letters AS letters_{h}_{w}"
        for h in range(height)
        for w in range(width)
        if h != w
    )


def _generate_col_d_join(width: int, height: int) -> str:
    return (
        "".join(
            f"JOIN entries AS col_entry_{h}_0 ON pair_id_{h}_0 = col_entry_{h}_0.pair_id\n"
            for h in range(1, height)
        )
        + "JOIN words AS col_0_word ON col_entry_1_0.word_id = col_0_word.id\n"
        + "\n".join(
            f"JOIN pairs AS pair_{h}_{w} ON pair_id_{h}_{w} = pair_{h}_{w}.id"
            for h in range(height)
            for w in range(width)
            if h != w
        )
    )


def _generate_col_d_where(width: int, height: int, minimum_frequency: int) -> str:
    return (
        f"WHERE col_0_word.minimum_frequency > {minimum_frequency}\n"
        + "".join(
            f"AND col_entry_1_0.word_id = col_entry_{h}_0.word_id\n"
            for h in range(2, height)
        )
        + "AND col_0_word.id NOT IN ("
        + ", ".join(
            [f"row_{h}_word_id" for h in range(height)]
            + [f"col_{w}_word_id" for w in range(1, width)]
        )
        + ")\n"
        + f"AND col_0_word.length = {height - 1}\n"
        + "\n".join(f"AND col_entry_{h}_0.position = {h - 1}" for h in range(1, height))
    )


def _generate_rows_cs(
    width: int, height: int, limit: int, minimum_frequency: int
) -> str:
    """
        This generates the query for Rows Cs which intersects with Column Bs:
        .. .. ..
        xx .. XX
        xx XX ..
    """
    inner_table = (
        "\n\t".join(
            _generate_cols_bs(width, height, limit, minimum_frequency).splitlines()
        )
        + "\n"
    )
    return f"""
SELECT
\t{_generate_rows_cs_select(width, height)}
FROM ({inner_table}) AS cols_bs
{_generate_rows_cs_join(width, height)}
{_generate_rows_cs_where(width, height, minimum_frequency)}
""" + _limit(
        limit * 10
    )


def _generate_rows_cs_select(width: int, height: int) -> str:
    return (
        "row_0_word_id,\n\t"
        + "".join(f"row_{h}_word.id AS row_{h}_word_id,\n\t" for h in range(1, height))
        + "".join(f"col_{w}_word_id,\n\t" for w in range(1, width))
        + "".join(
            f"row_entry_{h}_0.pair_id as pair_id_{h}_0,\n\t" for h in range(1, height)
        )
        + ",\n\t".join(
            f"pair_id_{h}_{w}" for h in range(height) for w in range(1, width) if h != w
        )
    )


def _generate_rows_cs_join(width: int, height: int) -> str:
    return "\n".join(_generate_rows_cs_join_batch(width, h) for h in range(1, height))


def _generate_rows_cs_join_batch(width: int, h: int) -> str:
    last = width - 2 if h == width - 1 else width - 1
    return (
        f"JOIN entries AS row_entry_{h}_{last} ON row_entry_{h}_{last}.pair_id = pair_id_{h}_{last}\n"
        + "".join(
            f"JOIN entries AS row_entry_{h}_{w} ON row_entry_{h}_{last}.word_id = row_entry_{h}_{w}.word_id\n"
            for w in range(last)
            if h != w
        )
        + f"JOIN words AS row_{h}_word ON row_entry_{h}_{last}.word_id = row_{h}_word.id"
    )


def _generate_rows_cs_where(width: int, height: int, minimum_frequency: int) -> str:
    previous_rows = ["row_0_word_id"] + [f"col_{w}_word_id" for w in range(1, width)]
    where_word_ids = []
    for h in range(1, height):
        row_id = f"row_{h}_word.id"
        where_word_ids.append(f"{row_id} NOT IN ({', '.join(previous_rows)})")
        previous_rows.append(row_id)
    return (
        "WHERE "
        + "\nAND ".join(where_word_ids)
        + "\n"
        + "\n".join(
            _generate_rows_cs_where_batch(width, h, minimum_frequency)
            for h in range(1, height)
        )
    )


def _generate_rows_cs_where_batch(width: int, h: int, minimum_frequency: int) -> str:
    last = width - 2 if h == width - 1 else width - 1
    return (
        f"AND row_{h}_word.minimum_frequency > {minimum_frequency}\n"
        + f"AND row_{h}_word.length = {width - 1}\n"
        + "".join(
            f"AND row_entry_{h}_{w}.position = {w - 1 if w > h else w}\n"
            for w in range(width)
            if h != w
        )
        + "\n".join(
            f"AND row_entry_{h}_{w}.pair_id = pair_id_{h}_{w}"
            for w in range(1, last)
            if h != w
        )
    )


def _generate_cols_bs(
    width: int, height: int, limit: int, minimum_frequency: int
) -> str:
    """
        This generates the query for Columns Bs which intersects with Row A:
        .. XX XX
        .. .. xx
        .. xx ..
    """
    inner_table = (
        "\n\t".join(_generate_row_a(width, limit, minimum_frequency).splitlines())
        + "\n"
    )
    return f"""
SELECT
\t{_generate_cols_bs_select(width, height)}
FROM ({inner_table}) AS row_0
{_generate_cols_bs_join(width, height)}
{_generate_cols_bs_where(width, height, minimum_frequency)}
""" + _limit(
        limit * 100
    )


def _generate_cols_bs_select(width: int, height: int) -> str:
    return (
        "row_0.word_id AS row_0_word_id,\n\t"
        + "".join(f"col_{w}_word.id AS col_{w}_word_id,\n\t" for w in range(1, width))
        + ",\n\t".join(
            f"col_entry_{h}_{w}.pair_id AS pair_id_{h}_{w}"
            for h in range(height)
            for w in range(1, width)
            if h != w
        )
    )


def _generate_cols_bs_join(width: int, height: int) -> str:
    return (
        "".join(
            f"JOIN entries AS col_entry_0_{w} ON row_0.pair_id_0_{w} = col_entry_0_{w}.pair_id\n"
            for w in range(1, width)
        )
        + "".join(
            f"JOIN words AS col_{w}_word ON col_entry_0_{w}.word_id = col_{w}_word.id\n"
            for w in range(1, width)
        )
        + "\n".join(
            f"JOIN entries AS col_entry_{h}_{w} ON col_entry_0_{w}.word_id = col_entry_{h}_{w}.word_id"
            for h in range(1, height)
            for w in range(1, width)
            if h != w
        )
    )


def _generate_cols_bs_where(width: int, height: int, minimum_frequency: int) -> str:
    return (
        "WHERE row_0.word_id NOT IN ("
        + ", ".join(f"col_{w}_word.id" for w in range(1, width))
        + ")\n"
        + "".join(
            f"AND col_{w}_word.minimum_frequency > {minimum_frequency}\n"
            for w in range(1, width)
        )
        + "".join(f"AND col_{w}_word.length = {height - 1}\n" for w in range(1, width))
        + "\n".join(
            f"AND col_entry_{h}_{w}.position = {h - 1 if h > w else h}"
            for h in range(height)
            for w in range(1, width)
            if h != w
        )
    )


def _generate_row_a(width: int, limit: int, minimum_frequency: int) -> str:
    """
        This generates the query for Row A which is the start of the grid:
        .. xx xx
        .. .. ..
        .. .. ..
    """
    return f"""
SELECT
\t{_generate_row_a_select(width)}
FROM words
{_generate_row_a_join(width)}
{_generate_row_a_where(width, minimum_frequency)}
""" + _limit(
        limit * 1000
    )


def _generate_row_a_select(width: int) -> str:
    return "words.id AS word_id,\n\t" + ",\n\t".join(
        f"row_entry_0_{w}.pair_id AS pair_id_0_{w}" for w in range(1, width)
    )


def _generate_row_a_join(width: int) -> str:
    return "\n".join(
        f"JOIN entries AS row_entry_0_{w} ON words.id = row_entry_0_{w}.word_id"
        for w in range(1, width)
    )


def _generate_row_a_where(width: int, minimum_frequency: int) -> str:
    return (
        f"WHERE words.minimum_frequency > {minimum_frequency}\n"
        + f"AND words.length = {width - 1}\n"
        + "\n".join(f"AND row_entry_0_{w}.position = {w - 1}" for w in range(1, width))
    )
