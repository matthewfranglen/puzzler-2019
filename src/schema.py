from __future__ import annotations
from pathlib import Path
from io import StringIO
from src.sql import connect, execute, select, vacuum


def create() -> None:
    create_tables()
    load_words()
    load_tables()
    vacuum()
    analyze_tables()


def create_tables() -> None:
    execute("""DROP TABLE IF EXISTS entries""")
    execute("""DROP TABLE IF EXISTS pairs""")
    execute("""DROP TABLE IF EXISTS words""")

    execute(
        """
CREATE TABLE words (
    id SERIAL PRIMARY KEY,
    word TEXT UNIQUE NOT NULL,
    length INTEGER,
    minimum_frequency INTEGER
)
    """
    )

    execute(
        """
CREATE TABLE pairs (
    id SERIAL PRIMARY KEY,
    letters TEXT UNIQUE NOT NULL,
    frequency INTEGER NOT NULL
)
    """
    )

    execute(
        """
CREATE TABLE entries (
    pair_id INTEGER REFERENCES pairs(id),
    word_id INTEGER REFERENCES words(id),
    position INTEGER,
    CONSTRAINT distinct_entries UNIQUE(pair_id, word_id, position)
)
    """
    )


def load_words() -> None:
    words = sorted(
        {
            word.lower()
            for word in Path("/usr/share/dict/words").read_text().splitlines()
        }
    )
    buffer = StringIO("\n".join(words))
    with connect() as cursor:
        cursor.copy_from(buffer, "words", columns=["word"])
    execute("""DELETE FROM words WHERE character_length(word) % 2 = 1""")
    execute("""UPDATE words SET length = character_length(word) / 2""")
    execute("""ALTER TABLE words ALTER COLUMN length SET NOT NULL""")
    execute("""CREATE INDEX ON words (length)""")


def load_tables() -> None:
    execute(
        """
WITH RECURSIVE word_to_pair(word_id, letters, remainder, index) AS (
        SELECT
            words.id AS word_id,
            substring(words.word from 0 for 3) AS letters,
            substring(words.word from 3) AS remainder,
            0 AS index
        FROM words
        WHERE character_length(words.word) % 2 = 0
    UNION ALL
        SELECT
            word_to_pair.word_id AS word_id,
            substring(word_to_pair.remainder from 0 for 3) AS letters,
            substring(word_to_pair.remainder from 3) AS remainder,
            word_to_pair.index + 1 AS index
        FROM word_to_pair
        WHERE word_to_pair.remainder != ''
),
pair_ids(pair_id, letters) AS (
    INSERT INTO pairs (letters, frequency)
        SELECT
            word_to_pair.letters AS letters,
            COUNT(*) AS frequency
        FROM word_to_pair
        GROUP BY word_to_pair.letters
        RETURNING pairs.id AS pair_id, pairs.letters AS letters
)
INSERT INTO entries (pair_id, word_id, position)
    SELECT
        pair_ids.pair_id AS pair_id,
        word_to_pair.word_id AS word_id,
        word_to_pair.index AS position
    FROM word_to_pair
    JOIN pair_ids ON word_to_pair.letters = pair_ids.letters
"""
    )
    execute("""CREATE INDEX ON entries (pair_id)""")
    execute("""CREATE INDEX ON entries (word_id)""")
    execute("""CREATE INDEX ON entries (position)""")
    execute("""CREATE INDEX ON pairs (frequency)""")

    while delete_single_pairs() == 1:
        pass

    execute(
        """
UPDATE words
SET minimum_frequency = (
    SELECT min(pairs.frequency)
    FROM entries
    JOIN pairs ON entries.pair_id = pairs.id
    WHERE entries.word_id = words.id
)
"""
    )
    execute("""CREATE INDEX ON words (minimum_frequency)""")


def delete_single_pairs() -> None:
    execute(
        """
WITH bad_pairs (id) AS (
    SELECT
        pairs.id
    FROM pairs
    WHERE pairs.frequency = 1
),
bad_words (id) AS (
    SELECT DISTINCT
        words.id
    FROM words
    JOIN entries ON entries.word_id = words.id
    JOIN pairs ON entries.pair_id = pairs.id
    WHERE pairs.frequency = 1
),
deleted_entries AS (
    DELETE FROM entries
    WHERE word_id IN (SELECT id FROM bad_words)
),
deleted_pairs AS (
    DELETE FROM pairs
    WHERE id IN (SELECT id FROM bad_pairs)
)
DELETE FROM words
WHERE id IN (SELECT id FROM bad_words)
"""
    )

    execute(
        """
UPDATE pairs
SET frequency = (
    SELECT COUNT(*)
    FROM entries
    WHERE entries.pair_id = pairs.id
)
"""
    )

    return select("SELECT min(frequency) FROM pairs")[0][0]


def analyze_tables() -> None:
    execute("""ANALYZE""")
