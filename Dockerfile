FROM postgres:10

RUN apt-get update \
      && apt-get install -y --no-install-recommends \
           postgresql-plpython3-10 \
      && rm -rf /var/lib/apt/lists/*
