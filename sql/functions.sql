CREATE OR REPLACE FUNCTION public.exec(TEXT)
    RETURNS SETOF RECORD
    LANGUAGE 'plpgsql'
AS $BODY$
BEGIN
    RETURN QUERY EXECUTE $1;
END
$BODY$;

CREATE EXTENSION plpython3u;

CREATE OR REPLACE FUNCTION generate_sql (width INTEGER, height INTEGER, _limit INTEGER, minimum_frequency INTEGER)
    RETURNS TEXT
AS $$
def _inner_table(sql: str):
    return "\n\t".join(sql.splitlines()) + "\n"

def _generate_sql(width: int, height: int, _limit: int, minimum_frequency: int):
    return """
SELECT
\t{_select}
FROM ({_table}) col_d
""".format(_select=_generate_outer_select(width, height), _table=_inner_table(_generate_col_d(width, height, _limit, minimum_frequency)))

def _generate_outer_select(width: int, height: int):
    return " || '\\n' || \n\t".join(
        " || ".join(
            "    '..'   " if w == h else "letters_{h}_{w}".format(h=h, w=w) for w in range(width)
        )
        for h in range(height)
    )


def _generate_col_d(width: int, height: int, _limit: int, minimum_frequency: int):
    """
        This generates the query for Column D which intersects with Rows Cs:
        .. .. ..
        XX .. ..
        XX .. ..
    """
    return """
SELECT
\t{}
FROM ({}) rows_cs
{}
{}
""".format(
    _generate_col_d_select(width, height),
    _inner_table(_generate_rows_cs(width, height, _limit, minimum_frequency)),
    _generate_col_d_join(width, height),
    _generate_col_d_where(width, height, minimum_frequency)
) + __limit(
    _limit
)


def __limit(_limit: int):
    return "LIMIT {}".format(_limit) if _limit > 0 else ""


def _generate_col_d_select(width: int, height: int):
    return ",\n\t".join(
        "pair_{h}_{w}.letters AS letters_{h}_{w}".format(h=h, w=w)
        for h in range(height)
        for w in range(width)
        if h != w
    )


def _generate_col_d_join(width: int, height: int):
    return (
        "".join(
            "JOIN entries AS col_entry_{h}_0 ON pair_id_{h}_0 = col_entry_{h}_0.pair_id\n".format(h=h)
            for h in range(1, height)
        )
        + "JOIN words AS col_0_word ON col_entry_1_0.word_id = col_0_word.id\n"
        + "\n".join(
            "JOIN pairs AS pair_{h}_{w} ON pair_id_{h}_{w} = pair_{h}_{w}.id".format(h=h, w=w)
            for h in range(height)
            for w in range(width)
            if h != w
        )
    )


def _generate_col_d_where(width: int, height: int, minimum_frequency: int):
    return (
        "WHERE col_0_word.minimum_frequency > {minimum_frequency}\n".format(minimum_frequency=minimum_frequency)
        + "".join(
            "AND col_entry_1_0.word_id = col_entry_{h}_0.word_id\n".format(h=h)
            for h in range(2, height)
        )
        + "AND col_0_word.id NOT IN ("
        + ", ".join(
            ["row_{h}_word_id".format(h=h) for h in range(height)]
            + ["col_{w}_word_id".format(w=w) for w in range(1, width)]
        )
        + ")\n"
        + "AND col_0_word.length = {}\n".format(height - 1)
        + "\n".join("AND col_entry_{}_0.position = {}".format(h, h-1) for h in range(1, height))
    )


def _generate_rows_cs(
    width: int, height: int, _limit: int, minimum_frequency: int
):
    """
        This generates the query for Rows Cs which intersects with Column Bs:
        .. .. ..
        xx .. XX
        xx XX ..
    """
    return """
SELECT
\t{}
FROM ({}) AS cols_bs
{}
{}
""".format(
    _generate_rows_cs_select(width, height),
    _inner_table(_generate_cols_bs(width, height, _limit, minimum_frequency)),
    _generate_rows_cs_join(width, height),
    _generate_rows_cs_where(width, height, minimum_frequency)
) + __limit(
        _limit * 10
    )


def _generate_rows_cs_select(width: int, height: int):
    return (
        "row_0_word_id,\n\t"
        + "".join("row_{h}_word.id AS row_{h}_word_id,\n\t".format(h=h) for h in range(1, height))
        + "".join("col_{w}_word_id,\n\t".format(w=w) for w in range(1, width))
        + "".join(
            "row_entry_{h}_0.pair_id as pair_id_{h}_0,\n\t".format(h=h) for h in range(1, height)
        )
        + ",\n\t".join(
            "pair_id_{h}_{w}".format(h=h, w=w) for h in range(height) for w in range(1, width) if h != w
        )
    )


def _generate_rows_cs_join(width: int, height: int):
    return "\n".join(_generate_rows_cs_join_batch(width, h) for h in range(1, height))


def _generate_rows_cs_join_batch(width: int, h: int):
    _last = width - 2 if h == width - 1 else width - 1
    return (
        "JOIN entries AS row_entry_{h}_{_last} ON row_entry_{h}_{_last}.pair_id = pair_id_{h}_{_last}\n".format(h=h, _last=_last)
        + "".join(
            "JOIN entries AS row_entry_{h}_{w} ON row_entry_{h}_{_last}.word_id = row_entry_{h}_{w}.word_id\n".format(h=h, w=w, _last=_last)
            for w in range(_last)
            if h != w
        )
        + "JOIN words AS row_{h}_word ON row_entry_{h}_{_last}.word_id = row_{h}_word.id".format(h=h, _last=_last)
    )


def _generate_rows_cs_where(width: int, height: int, minimum_frequency: int):
    previous_rows = ["row_0_word_id"] + ["col_{w}_word_id".format(w=w) for w in range(1, width)]
    where_word_ids = []
    for h in range(1, height):
        row_id = "row_{h}_word.id".format(h=h)
        where_word_ids.append("{} NOT IN ({})".format(row_id, ', '.join(previous_rows)))
        previous_rows.append(row_id)
    return (
        "WHERE "
        + "\nAND ".join(where_word_ids)
        + "\n"
        + "\n".join(
            _generate_rows_cs_where_batch(width, h, minimum_frequency)
            for h in range(1, height)
        )
    )


def _generate_rows_cs_where_batch(width: int, h: int, minimum_frequency: int):
    _last = width - 2 if h == width - 1 else width - 1
    return (
        "AND row_{h}_word.minimum_frequency > {minimum_frequency}\n".format(h=h, minimum_frequency=minimum_frequency)
        + "AND row_{}_word.length = {}\n".format(h, width-1)
        + "".join(
            "AND row_entry_{}_{}.position = {}\n".format(h, w, w - 1 if w > h else w)
            for w in range(width)
            if h != w
        )
        + "\n".join(
            "AND row_entry_{h}_{w}.pair_id = pair_id_{h}_{w}".format(h=h, w=w)
            for w in range(1, _last)
            if h != w
        )
    )


def _generate_cols_bs(
    width: int, height: int, _limit: int, minimum_frequency: int
):
    """
        This generates the query for Columns Bs which intersects with Row A:
        .. XX XX
        .. .. xx
        .. xx ..
    """
    return """
SELECT
\t{}
FROM ({}) AS row_0
{}
{}
""".format(
    _generate_cols_bs_select(width, height),
    _inner_table(_generate_row_a(width, _limit, minimum_frequency)),
    _generate_cols_bs_join(width, height),
    _generate_cols_bs_where(width, height, minimum_frequency)
) + __limit(
        _limit * 100
    )


def _generate_cols_bs_select(width: int, height: int):
    return (
        "row_0.word_id AS row_0_word_id,\n\t"
        + "".join("col_{w}_word.id AS col_{w}_word_id,\n\t".format(w=w) for w in range(1, width))
        + ",\n\t".join(
            "col_entry_{h}_{w}.pair_id AS pair_id_{h}_{w}".format(h=h, w=w)
            for h in range(height)
            for w in range(1, width)
            if h != w
        )
    )


def _generate_cols_bs_join(width: int, height: int):
    return (
        "".join(
            "JOIN entries AS col_entry_0_{w} ON row_0.pair_id_0_{w} = col_entry_0_{w}.pair_id\n".format(w=w)
            for w in range(1, width)
        )
        + "".join(
            "JOIN words AS col_{w}_word ON col_entry_0_{w}.word_id = col_{w}_word.id\n".format(w=w)
            for w in range(1, width)
        )
        + "\n".join(
            "JOIN entries AS col_entry_{h}_{w} ON col_entry_0_{w}.word_id = col_entry_{h}_{w}.word_id".format(h=h, w=w)
            for h in range(1, height)
            for w in range(1, width)
            if h != w
        )
    )


def _generate_cols_bs_where(width: int, height: int, minimum_frequency: int):
    return (
        "WHERE row_0.word_id NOT IN ("
        + ", ".join("col_{w}_word.id".format(w=w) for w in range(1, width))
        + ")\n"
        + "".join(
            "AND col_{w}_word.minimum_frequency > {minimum_frequency}\n".format(w=w, minimum_frequency=minimum_frequency)
            for w in range(1, width)
        )
        + "".join("AND col_{}_word.length = {}\n".format(w, height-1) for w in range(1, width))
        + "\n".join(
            "AND col_entry_{}_{}.position = {}".format(h, w, h - 1 if h > w else h)
            for h in range(height)
            for w in range(1, width)
            if h != w
        )
    )


def _generate_row_a(width: int, _limit: int, minimum_frequency: int):
    """
        This generates the query for Row A which is the start of the grid:
        .. xx xx
        .. .. ..
        .. .. ..
    """
    return """
SELECT
\t{}
FROM words
{}
{}
""".format(
    _generate_row_a_select(width),
    _generate_row_a_join(width),
    _generate_row_a_where(width, minimum_frequency)
) + __limit(
        _limit * 1000
    )


def _generate_row_a_select(width: int):
    return "words.id AS word_id,\n\t" + ",\n\t".join(
        "row_entry_0_{w}.pair_id AS pair_id_0_{w}".format(w=w) for w in range(1, width)
    )


def _generate_row_a_join(width: int):
    return "\n".join(
        "JOIN entries AS row_entry_0_{w} ON words.id = row_entry_0_{w}.word_id".format(w=w)
        for w in range(1, width)
    )


def _generate_row_a_where(width: int, minimum_frequency: int):
    return (
        "WHERE words.minimum_frequency > {minimum_frequency}\n".format(minimum_frequency=minimum_frequency)
        + "AND words.length = {}\n".format(width - 1)
        + "\n".join("AND row_entry_0_{}.position = {}".format(w, w-1) for w in range(1, width))
    )

return _generate_sql(width, height, _limit, minimum_frequency)

$$ LANGUAGE plpython3u;
