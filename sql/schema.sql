-- CREATE TABLES

DROP TABLE IF EXISTS entries;
DROP TABLE IF EXISTS pairs;
DROP TABLE IF EXISTS words;

CREATE TABLE words (
    id SERIAL PRIMARY KEY,
    word TEXT NOT NULL,
    length INTEGER,
    minimum_frequency INTEGER
);

CREATE TABLE pairs (
    id SERIAL PRIMARY KEY,
    letters TEXT UNIQUE NOT NULL,
    frequency INTEGER NOT NULL
);

CREATE TABLE entries (
    pair_id INTEGER REFERENCES pairs(id),
    word_id INTEGER REFERENCES words(id),
    position INTEGER,
    CONSTRAINT distinct_entries UNIQUE(pair_id, word_id, position)
);

-- LOAD WORDS FROM FILE

\copy words(word) FROM '/usr/share/dict/words' WITH CSV;
UPDATE words SET word = lower(word);

-- delete duplicates see https://stackoverflow.com/a/12963112
DELETE FROM words a USING (
      SELECT MIN(ctid) as ctid, word
        FROM words
        GROUP BY word HAVING COUNT(*) > 1
      ) b
      WHERE a.word = b.word
      AND a.ctid <> b.ctid;
ALTER TABLE words ADD CONSTRAINT unique_word UNIQUE (word);

DELETE FROM words WHERE character_length(word) % 2 = 1;
UPDATE words SET length = character_length(word) / 2;
ALTER TABLE words ALTER COLUMN length SET NOT NULL;
CREATE INDEX ON words (length);

-- LOAD PAIRS AND ENTRIES

WITH RECURSIVE word_to_pair(word_id, letters, remainder, index) AS (
    SELECT
    words.id AS word_id,
    substring(words.word from 0 for 3) AS letters,
    substring(words.word from 3) AS remainder,
    0 AS index
    FROM words
    WHERE character_length(words.word) % 2 = 0
    UNION ALL
    SELECT
    word_to_pair.word_id AS word_id,
    substring(word_to_pair.remainder from 0 for 3) AS letters,
    substring(word_to_pair.remainder from 3) AS remainder,
    word_to_pair.index + 1 AS index
    FROM word_to_pair
    WHERE word_to_pair.remainder != ''
),
pair_ids(pair_id, letters) AS (
    INSERT INTO pairs (letters, frequency)
    SELECT
    word_to_pair.letters AS letters,
    COUNT(*) AS frequency
    FROM word_to_pair
    GROUP BY word_to_pair.letters
    RETURNING pairs.id AS pair_id, pairs.letters AS letters
)
INSERT INTO entries (pair_id, word_id, position)
SELECT
pair_ids.pair_id AS pair_id,
word_to_pair.word_id AS word_id,
word_to_pair.index AS position
FROM word_to_pair
JOIN pair_ids ON word_to_pair.letters = pair_ids.letters;

CREATE INDEX ON entries (pair_id);
CREATE INDEX ON entries (word_id);
CREATE INDEX ON entries (position);
CREATE INDEX ON pairs (frequency);

-- DELETE LOW FREQUENCY PAIRS, WORDS

WITH bad_pairs (id) AS (
    SELECT
    pairs.id
    FROM pairs
    WHERE pairs.frequency = 1
),
bad_words (id) AS (
    SELECT DISTINCT
    words.id
    FROM words
    JOIN entries ON entries.word_id = words.id
    JOIN pairs ON entries.pair_id = pairs.id
    WHERE pairs.frequency = 1
),
deleted_entries AS (
    DELETE FROM entries
    WHERE word_id IN (SELECT id FROM bad_words)
),
deleted_pairs AS (
    DELETE FROM pairs
    WHERE id IN (SELECT id FROM bad_pairs)
)
DELETE FROM words
WHERE id IN (SELECT id FROM bad_words);

UPDATE pairs
SET frequency = (
    SELECT COUNT(*)
    FROM entries
    WHERE entries.pair_id = pairs.id
);

WITH bad_pairs (id) AS (
    SELECT
    pairs.id
    FROM pairs
    WHERE pairs.frequency = 1
),
bad_words (id) AS (
    SELECT DISTINCT
    words.id
    FROM words
    JOIN entries ON entries.word_id = words.id
    JOIN pairs ON entries.pair_id = pairs.id
    WHERE pairs.frequency = 1
),
deleted_entries AS (
    DELETE FROM entries
    WHERE word_id IN (SELECT id FROM bad_words)
),
deleted_pairs AS (
    DELETE FROM pairs
    WHERE id IN (SELECT id FROM bad_pairs)
)
DELETE FROM words
WHERE id IN (SELECT id FROM bad_words);

UPDATE pairs
SET frequency = (
    SELECT COUNT(*)
    FROM entries
    WHERE entries.pair_id = pairs.id
);

UPDATE words
SET minimum_frequency = (
    SELECT min(pairs.frequency)
    FROM entries
    JOIN pairs ON entries.pair_id = pairs.id
    WHERE entries.word_id = words.id
);
CREATE INDEX ON words (minimum_frequency);

VACUUM FULL;
ANALYZE;
